import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { LoanService } from '../_services/loan.service';
import { CustomValidatorService } from '../_services/custom-validator.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {
  calcForm!: FormGroup;

  isSuccessful = false;
  submitted = false;
  errorMessage = '';

  constructor(
    private fb: FormBuilder,
    private loanService: LoanService,
    private customValidator: CustomValidatorService) { }

  ngOnInit(): void {
    this.calcForm = this.fb.group({
      monthlyIncome: [510000, [ Validators.required, Validators.min(500000)], this.customValidator.validNumber],
      requestedAmount: [21000000, [ Validators.required, Validators.min(20000000)], this.customValidator.validNumber],
      loanTerm: [50, [ Validators.required, Validators.min(36), Validators.max(360)], this.customValidator.validNumber],
      children: ['', [ Validators.required]],
      coapplicant: ['', [ Validators.required]],
   });
  }

  get cf() { return this.calcForm.controls; }

  onSubmit(): void {
    this.submitted = true;
    if (this.calcForm.valid) {
      console.log(this.calcForm.value);
      const { monthlyIncome, requestedAmount, loanTerm, children, coapplicant } = this.calcForm.value;
      this.loanService.validateLoan(monthlyIncome, requestedAmount, loanTerm, children, coapplicant).subscribe(
        data => {
          console.log(data);
          this.isSuccessful = true;
        },
        err => {
          console.log(err);
          this.errorMessage = err.error.message;
        }
      );
    } 
  }

}
