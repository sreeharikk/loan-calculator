import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable({ providedIn: 'root' })
export class CustomValidatorService {

  constructor() { }

  validNumber(control: FormControl) {
    return new Promise(resolve => {
      const numPattern = /^[0-9]*$/;

      if (!numPattern.test(control.value)) {
        resolve({ invalidValue: true });
      }
      resolve(null);
    });
  }
}