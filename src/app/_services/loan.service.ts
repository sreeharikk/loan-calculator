import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URI = 'https://homework.fdp.workers.dev/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class LoanService {

  constructor(private http: HttpClient) { }

  validateLoan(
    monthlyIncome: number, 
    requestedAmount: number, 
    loanTerm: number, 
    children: string,
    coapplicant: string): Observable<any> {
    return this.http.post(API_URI, {
      monthlyIncome,
      requestedAmount,
      loanTerm,
      children,
      coapplicant
    }, httpOptions);
  }
}
